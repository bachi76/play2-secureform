import ch.insign.commons.db.SecureForm;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.FormElement;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Test;
import play.db.ebean.Model;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Copyright (C) 2014 by insign gmbh - http://www.insign.ch
 *
 * @author Martin Bachmann (https://plus.google.com/+MartinBachmann)
 */

public class SecureFormTest {

    class Entity extends Model {}

    @Test
    public void signFormsNoGETTest() {

        String action = "/some/url";
        String[] expectedFields = {"element_1", "element_2", "element_3_1", "element_3_2", "element_4_1", "element_4_2", "element_4_3", "element_4_4", "element_5", "element_6", "element_7", "element_7", "element_7", "element_8_1", "element_8_2", "element_8_3", "element_9", "form_id", "submit"};
        runSignTest(action, "", null, expectedFields);
    }

    @Test
    public void signFormsWith1GETTest() {

        String action = "/some/url?getparam1=5";
        String[] expectedFields = {"element_1", "element_2", "element_3_1", "element_3_2", "element_4_1", "element_4_2", "element_4_3", "element_4_4", "element_5", "element_6", "element_7", "element_7", "element_7", "element_8_1", "element_8_2", "element_8_3", "element_9", "form_id", "getparam1", "submit"};
        runSignTest(action, "", "", expectedFields);
    }

    @Test
    public void signFormsWithSomeGETTest() {
        String action = "/some/url?getparam1=5&getparam2=xxx&getWithoutValue";
        String[] expectedFields = {"element_1", "element_2", "element_3_1", "element_3_2", "element_4_1", "element_4_2", "element_4_3", "element_4_4", "element_5", "element_6", "element_7", "element_7", "element_7", "element_8_1", "element_8_2", "element_8_3", "element_9", "form_id", "getWithoutValue", "getparam1", "getparam2", "submit"};
        runSignTest(action, "", null,  expectedFields);
    }

    @Test
    public void signFormsWithGetNoVal() {
        String action = "/some/url?getWithoutValue";
        String[] expectedFields = {"element_1", "element_2", "element_3_1", "element_3_2", "element_4_1", "element_4_2", "element_4_3", "element_4_4", "element_5", "element_6", "element_7", "element_7", "element_7", "element_8_1", "element_8_2", "element_8_3", "element_9", "form_id", "getWithoutValue", "submit"};
        runSignTest(action, "", "", expectedFields);
    }

    @Test
    public void signFormsWithAllowedFields1Test() {

        String action = "/some/url";
        String[] expectedFields = {"element_1", "element_2", "element_3_1", "element_3_2", "element_4_1", "element_4_2", "element_4_3", "element_4_4", "element_5", "element_6", "element_7", "element_7", "element_7", "element_8_1", "element_8_2", "element_8_3", "element_9", "extra*", "form_id", "submit"};
        runSignTest(action, "extra*", null, expectedFields);
    }

    @Test
    public void signFormsWithAllowedFields2Test() {

        String action = "/some/url";
        String[] expectedFields = {"element_1", "element_2", "element_3_1", "element_3_2", "element_4_1", "element_4_2", "element_4_3", "element_4_4", "element_5", "element_6", "element_7", "element_7", "element_7", "element_8_1", "element_8_2", "element_8_3", "element_9", "extra1", "extra2*", "form_id", "submit"};
        runSignTest(action, " extra1, extra2* ", "", expectedFields);
    }

    @Test
    public void signFormsWithAllowedFields3Test() {

        String action = "/some/url";
        String[] expectedFields = {"element_1", "element_2", "element_3_1", "element_3_2", "element_4_1", "element_4_2", "element_4_3", "element_4_4", "element_5", "element_6", "element_7", "element_7", "element_7", "element_8_1", "element_8_2", "element_8_3", "element_9", "extra1", "extra2*", "form_id", "submit"};
        runSignTest(action, " extra1", "extra2*", expectedFields);
    }

    @Test
    public void validateFormSubmission() {
        String action = "/some/url";
        String[] expectedFields = {"element_1", "element_2", "element_3_1", "element_3_2", "element_4_1", "element_4_2", "element_4_3", "element_4_4", "element_5", "element_6", "element_7", "element_7", "element_7", "element_8_1", "element_8_2", "element_8_3", "element_9", "form_id", "submit"};
        Map<String, String> data = generateSubmissionData(expectedFields);

        try {
            String sig = SecureForm.generateFormKey(Arrays.asList(expectedFields));
            data.put(SecureForm.SIG_FIELD_NAME, sig);
            SecureForm.validateSubmittedForm(data, null, new Entity());

        } catch (IOException | InvalidKeyException e) {
            Assert.fail(e.getMessage());
        }
    }



    @Test
    public void validateFormSubmissionWildcard() {
        String action = "/some/url";
        String[] formFields = {"element_1", "element_2", "element_3*", "e.?ement_4_1", "*lement_4_2", "eleme*_4_3", "element_4_4", "element_5", "element_6", "element_7", "element_7", "element_7", "element_8_1", "element_8_2", "element_8_3", "element_9", "form_id", "submit"};
        String[] submittedFields = {SecureForm.SIG_FIELD_ADD_FIELDS, SecureForm.SIG_FIELD_NAME, "element_1", "element_2", "element_3_1", "element_3_2", "element_3_3", "element_4_1", "element_4_2", "element_4_3", "element_4_4", "element_5", "element_6", "element_7", "element_7", "element_7", "element_8_1", "element_8_2", "element_8_3", "element_9", "form_id", "submit"};
        Map<String, String> data = generateSubmissionData(submittedFields);

        try {
            String sig = SecureForm.generateFormKey(Arrays.asList(formFields));
            data.put(SecureForm.SIG_FIELD_NAME, sig);
            SecureForm.validateSubmittedForm(data, null, new Entity());

        } catch (IOException | InvalidKeyException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void validateFormSubmissionAutoBrackets() {
        String action = "/some/url";
        String[] formFields = {"element1", "element2[]", "submit"};
        String[] submittedFields = {"element1", "element2[0]", "element2[1]", "submit"};
        Map<String, String> data = generateSubmissionData(submittedFields);

        try {
            String sig = SecureForm.generateFormKey(Arrays.asList(formFields));
            data.put(SecureForm.SIG_FIELD_NAME, sig);
            SecureForm.validateSubmittedForm(data, null, new Entity());

        } catch (IOException | InvalidKeyException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void validateFormSubmissionFieldAdded() {
        String action = "/some/url";
        String[] expectedFields = {"element_1", "element_2*", "element_3_1", "element_3_2", "element_4_1", "element_4_2", "element_4_3", "element_4_4", "element_5", "element_6", "element_7", "element_7", "element_7", "element_8_1", "element_8_2", "element_8_3", "element_9", "form_id", "submit"};
        Map<String, String> data = generateSubmissionData(expectedFields);

        try {
            String sig = SecureForm.generateFormKey(Arrays.asList(expectedFields));
            data.put("rogueField", "evilValue");
            data.put(SecureForm.SIG_FIELD_NAME, sig);
            SecureForm.validateSubmittedForm(data, null, new Entity());

        } catch (InvalidKeyException e) {
            // good!
            return;

        } catch (IOException e) {

            Assert.fail(e.getMessage());
            e.printStackTrace();
        }

        Assert.fail("SecureForm did not spot rogueField!");
    }

    @Test
    public void validateFormSubmissionKeyNotSet() {
        String action = "/some/url";
        String[] expectedFields = {"element_1", "element_2", "element_3_1", "element_3_2", "element_4_1", "element_4_2", "element_4_3", "element_4_4", "element_5", "element_6", "element_7", "element_7", "element_7", "element_8_1", "element_8_2", "element_8_3", "element_9", "form_id", "submit"};
        Map<String, String> data = generateSubmissionData(expectedFields);

        try {
            SecureForm.validateSubmittedForm(data, null, new Entity());

        } catch (InvalidKeyException e) {
            // good!
            return;
        }

        Assert.fail("SecureForm did not spot rogueField!");
    }



    // TODO: Add test with multiple forms on 1 page

    private void runSignTest(String action, String extraAllowedFields, String separateExtraAllowedFields, String[] expectedFields) {

        String field = ch.insign.commons.db.html.formKey.render(extraAllowedFields).toString();
        String html = getForm(action, field, separateExtraAllowedFields);
        String signedForm = SecureForm.signForms(html);

        Document doc = Jsoup.parse(signedForm);
        Elements elements = doc.getElementsByTag("form");
        List<FormElement> forms = elements.forms();

        for (FormElement form : forms) {
            String keyVal = form.getElementsByAttributeValue("name", SecureForm.SIG_FIELD_NAME).attr("value");
            try {
                List<String> fields = SecureForm.decodeFormKey(keyVal);
                Assert.assertArrayEquals("All fields found", expectedFields, fields.toArray());

            } catch (InvalidKeyException e) {
                Assert.fail("Decoding the formKey failed: " + e.getMessage());
            }
        }
    }


    private Map<String, String> generateSubmissionData(String[] fields) {
        Map<String, String> data = new HashMap<>();
        for (String key : fields) {
            data.put(key, "...");
        }
        return data;
    }

    private String getForm(String action, String formKey, String separateExtraAllowedFields) {
        String separateHiddenField = "";

        if (separateExtraAllowedFields != null) {
            separateHiddenField = "<input type=\"hidden\" name=\"" + SecureForm.SIG_FIELD_ADD_FIELDS +"\" value=\"" + separateExtraAllowedFields + "\">";
        }

        return "\n" +
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                "<head>\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
                "<title>Test Form</title>\n" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"./colors/color1/view.css\" media=\"all\">\n" +
                "<script type=\"text/javascript\" src=\"js/view.js\"></script>\n" +
                "<script type=\"text/javascript\" src=\"js/calendar.js\"></script>\n" +
                "</head>\n" +
                "<body id=\"main_body\" >\n" +
                "\t\n" +
                "\t<img id=\"top\" src=\"images/top.png\" alt=\"\">\n" +
                "\t<div id=\"form_container\">\n" +
                "\t\n" +
                "\t\t<h1><a>Test Form</a></h1>\n" +
                "\t\t<form id=\"form_804912\" class=\"appnitro\" enctype=\"multipart/form-data\" method=\"post\" action=\"" + action + "\">\n" +
                "\t\t\t\t\t<div class=\"form_description\">\n" +
                "\t\t\t<h2>Test Form</h2>\n" +
                "\t\t\t<p>Generated with http://www.phpform.org/formbuilder/</p>\n" +
                formKey +
                "\t\t</div>\t\t\t\t\t\t\n" +
                separateHiddenField +
                "\t\t\t<ul >\n" +
                "\t\t\t\n" +
                "\t\t\t\t\t<li id=\"li_1\" >\n" +
                "\t\t<label class=\"description\" for=\"element_1\">Text </label>\n" +
                "\t\t<div>\n" +
                "\t\t\t<input id=\"element_1\" name=\"element_1\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\"\"/> \n" +
                "\t\t</div> \n" +
                "\t\t</li>\t\t<li id=\"li_2\" >\n" +
                "\t\t<label class=\"description\" for=\"element_2\">Paragraph </label>\n" +
                "\t\t<div>\n" +
                "\t\t\t<textarea id=\"element_2\" name=\"element_2\" class=\"element textarea medium\"></textarea> \n" +
                "\t\t</div> \n" +
                "\t\t</li>\t\t<li id=\"li_7\" >\n" +
                "\t\t<label class=\"description\" for=\"element_7\">Multiple Choice </label>\n" +
                "\t\t<span>\n" +
                "\t\t\t<input id=\"element_7_1\" name=\"element_7\" class=\"element radio\" type=\"radio\" value=\"1\" />\n" +
                "<label class=\"choice\" for=\"element_7_1\">First option</label>\n" +
                "<input id=\"element_7_2\" name=\"element_7\" class=\"element radio\" type=\"radio\" value=\"2\" />\n" +
                "<label class=\"choice\" for=\"element_7_2\">Second option</label>\n" +
                "<input id=\"element_7_3\" name=\"element_7\" class=\"element radio\" type=\"radio\" value=\"3\" />\n" +
                "<label class=\"choice\" for=\"element_7_3\">Third option</label>\n" +
                "\n" +
                "\t\t</span> \n" +
                "\t\t</li>\t\t<li id=\"li_3\" >\n" +
                "\t\t<label class=\"description\" for=\"element_3\">Name </label>\n" +
                "\t\t<span>\n" +
                "\t\t\t<input id=\"element_3_1\" name= \"element_3_1\" class=\"element text\" maxlength=\"255\" size=\"8\" value=\"\"/>\n" +
                "\t\t\t<label>First</label>\n" +
                "\t\t</span>\n" +
                "\t\t<span>\n" +
                "\t\t\t<input id=\"element_3_2\" name= \"element_3_2\" class=\"element text\" maxlength=\"255\" size=\"14\" value=\"\"/>\n" +
                "\t\t\t<label>Last</label>\n" +
                "\t\t</span> \n" +
                "\t\t</li>\t\t<li id=\"li_4\" >\n" +
                "\t\t<label class=\"description\" for=\"element_4\">Time </label>\n" +
                "\t\t<span>\n" +
                "\t\t\t<input id=\"element_4_1\" name=\"element_4_1\" class=\"element text \" size=\"2\" type=\"text\" maxlength=\"2\" value=\"\"/> : \n" +
                "\t\t\t<label>HH</label>\n" +
                "\t\t</span>\n" +
                "\t\t<span>\n" +
                "\t\t\t<input id=\"element_4_2\" name=\"element_4_2\" class=\"element text \" size=\"2\" type=\"text\" maxlength=\"2\" value=\"\"/> : \n" +
                "\t\t\t<label>MM</label>\n" +
                "\t\t</span>\n" +
                "\t\t<span>\n" +
                "\t\t\t<input id=\"element_4_3\" name=\"element_4_3\" class=\"element text \" size=\"2\" type=\"text\" maxlength=\"2\" value=\"\"/>\n" +
                "\t\t\t<label>SS</label>\n" +
                "\t\t</span>\n" +
                "\t\t<span>\n" +
                "\t\t\t<select class=\"element select\" style=\"width:4em\" id=\"element_4_4\" name=\"element_4_4\">\n" +
                "\t\t\t\t<option value=\"AM\" >AM</option>\n" +
                "\t\t\t\t<option value=\"PM\" >PM</option>\n" +
                "\t\t\t</select>\n" +
                "\t\t\t<label>AM/PM</label>\n" +
                "\t\t</span> \n" +
                "\t\t</li>\t\t<li id=\"li_5\" >\n" +
                "\t\t<label class=\"description\" for=\"element_5\">Number </label>\n" +
                "\t\t<div>\n" +
                "\t\t\t<input id=\"element_5\" name=\"element_5\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\"\"/> \n" +
                "\t\t</div> \n" +
                "\t\t</li>\t\t<li id=\"li_8\" >\n" +
                "\t\t<label class=\"description\" for=\"element_8\">Checkboxes </label>\n" +
                "\t\t<span>\n" +
                "\t\t\t<input id=\"element_8_1\" name=\"element_8_1\" class=\"element checkbox\" type=\"checkbox\" value=\"1\" />\n" +
                "<label class=\"choice\" for=\"element_8_1\">First option</label>\n" +
                "<input id=\"element_8_2\" name=\"element_8_2\" class=\"element checkbox\" type=\"checkbox\" value=\"1\" />\n" +
                "<label class=\"choice\" for=\"element_8_2\">Second option</label>\n" +
                "<input id=\"element_8_3\" name=\"element_8_3\" class=\"element checkbox\" type=\"checkbox\" value=\"1\" />\n" +
                "<label class=\"choice\" for=\"element_8_3\">Third option</label>\n" +
                "\n" +
                "\t\t</span> \n" +
                "\t\t</li>\t\t<li id=\"li_9\" >\n" +
                "\t\t<label class=\"description\" for=\"element_9\">Drop Down </label>\n" +
                "\t\t<div>\n" +
                "\t\t<select class=\"element select medium\" id=\"element_9\" name=\"element_9\"> \n" +
                "\t\t\t<option value=\"\" selected=\"selected\"></option>\n" +
                "<option value=\"1\" >First option</option>\n" +
                "<option value=\"2\" >Second option</option>\n" +
                "<option value=\"3\" >Third option</option>\n" +
                "\n" +
                "\t\t</select>\n" +
                "\t\t</div> \n" +
                "\t\t</li>\t\t<li id=\"li_6\" >\n" +
                "\t\t<label class=\"description\" for=\"element_6\">Upload a File </label>\n" +
                "\t\t<div>\n" +
                "\t\t\t<input id=\"element_6\" name=\"element_6\" class=\"element file\" type=\"file\"/> \n" +
                "\t\t</div>  \n" +
                "\t\t</li>\n" +
                "\t\t\t\n" +
                "\t\t\t\t\t<li class=\"buttons\">\n" +
                "\t\t\t    <input type=\"hidden\" name=\"form_id\" value=\"804912\" />\n" +
                "\t\t\t    \n" +
                "\t\t\t\t<input id=\"saveForm\" class=\"button_text\" type=\"submit\" name=\"submit\" value=\"Submit\" />\n" +
                "\t\t</li>\n" +
                "\t\t\t</ul>\n" +
                "\t\t</form>\t\n" +
                "\t\t<div id=\"footer\">\n" +
                "\t\t\tGenerated by <a href=\"http://www.phpform.org\">pForm</a>\n" +
                "\t\t</div>\n" +
                "\t</div>\n" +
                "\t<img id=\"bottom\" src=\"images/bottom.png\" alt=\"\">\n" +
                "\t</body>\n" +
                "</html>";
    }

}
