package controllers;

import ch.insign.commons.db.SecureForm;
import models.Computer;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.createForm;
import views.html.editForm;
import views.html.list;

import static play.data.Form.form;

/**
 * Manage a database of computers
 */
public class Application extends Controller {

    /**
     * This result directly redirect to application home.
     */
    public static Result GO_HOME = redirect(
            routes.Application.list(0, "name", "asc", "")
    );

    /**
     * Handle default path requests, redirect to computers list
     */
    public static Result index() {
        return GO_HOME;
    }

    /**
     * Display the paginated list of computers.
     *
     * @param page Current page number (starts from 0)
     * @param sortBy Column to be sorted
     * @param order Sort order (either asc or desc)
     * @param filter Filter applied on computer names
     */
    public static Result list(int page, String sortBy, String order, String filter) {
        return ok(
                list.render(
                        Computer.page(page, 10, sortBy, order, filter),
                        sortBy, order, filter
                )
        );
    }

    /**
     * Display the 'edit form' of a existing Computer.
     *
     * @param id Id of the computer to edit
     */
    public static Result edit(Long id) {
        Form<Computer> computerForm = form(Computer.class).fill(
                Computer.find.byId(id)
        );
        return ok(

            /** Computes and adds the formKey from the rendered html forms **/
            SecureForm.signForms(
                editForm.render(id, computerForm)
            )
        );
    }

    /**
     * Handle the 'edit form' submission 
     *
     * @param id Id of the computer to edit
     */
    public static Result update(Long id) {

        // Note: You could also use Play's allowedFields param - this would silently
        // ignore sent but not allowed fields:
        // String allowedFields[] = {"name", "company.id", "introduced", "discontinued"};
        // Form<Computer> computerForm = SecureForm.form(Computer.class).bindFromRequest(allowedFields);

        Form<Computer> computerForm = SecureForm.form(Computer.class).bindFromRequest();
        if(computerForm.hasErrors()) {
            return badRequest(
                SecureForm.signForms(
                    editForm.render(id, computerForm)
                )
            );
        }
        computerForm.get().update(id);
        flash("success", "Computer " + computerForm.get().name + " has been updated");
        return GO_HOME;
    }

    /**
     * Display the 'new computer form'.
     */
    public static Result create() {
        Form<Computer> computerForm = form(Computer.class);
        return ok(
                createForm.render(computerForm)
        );
    }

    /**
     * Handle the 'new computer form' submission 
     */
    public static Result save() {
        Form<Computer> computerForm = form(Computer.class).bindFromRequest();
        if(computerForm.hasErrors()) {
            return badRequest(createForm.render(computerForm));
        }
        computerForm.get().save();
        flash("success", "Computer " + computerForm.get().name + " has been created");
        return GO_HOME;
    }

    /**
     * Handle computer deletion
     */
    public static Result delete(Long id) {
        Computer.find.ref(id).delete();
        flash("success", "Computer has been deleted");
        return GO_HOME;
    }


}
            
