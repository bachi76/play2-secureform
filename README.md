
Play Framework SecureForm
=========================
securing your Play Framework form-to-entity binding
---------------------------------------------------

The problem
-----------

The [Play 2 Framework] offers an easy way to bind form submissions to persistent model entities (usually Ebean or JPA-based).

Example:

    :::java
    /**
     * Handle the 'new computer form' submission
     */
    public static Result save() {
        Form<Computer> computerForm = form(Computer.class).bindFromRequest();
        if(computerForm.hasErrors()) {
            return badRequest(createForm.render(computerForm));
        }
        computerForm.get().save();
        return GO_HOME;
    }


The code looks elegant but exposes a big security hole: [Parameter tampering]. An attacker who manipulates the
form (e.g. adds a field) will be able to set any entity field, regardless of whether it was made available in the form,
since the form submission is directly bound to the backing entity. Think of something like "isAdmin" and you get the picture.

This has been discussed here: https://github.com/playframework/playframework/issues/2358

Usual solutions
---------------
You could either create a mapping in code which maps form submission fields to entity attributes. Or you could use the
allowedFields parameter in bindFromRequest() to define the set of allowed fields.

Both methods require you to define the editable fields twice: Once in the form, and once in the mapping or allowedFields array.
This can be tedious and error prone when dealing with many and large forms.

The SecureForm solution
-----------------------

Yet another approach is to use a form signature which lists and signs the field list of the form sent to the client. When
receiving the form submission, the submitted data is checked vs. the form signature, and altered forms are rejected.

SecureForm provides this approach.

Usage
-----
1) Add the @formKey() helper to your form (it just adds a hidden field)

2) Sign your form on output

    :::java
    public static Result edit(Long id) {
        Form<Computer> computerForm = form(Computer.class).fill(
                Computer.find.byId(id)
        );
        return ok(

            /** Computes and adds the formKey from the rendered html forms **/
            SecureForm.signForms(
                editForm.render(id, computerForm)
            )
        );
    }


3) Use SecureForm instead of Form when binding form submissions

    :::java
    /**
     * Handle the 'edit form' submission
     *
     * @param id Id of the computer to edit
     */
    public static Result update(Long id) {

        // Note: You could also use Play's allowedFields param - this would silently
        // ignore sent but not allowed fields:
        // String allowedFields[] = {"name", "company.id", "introduced", "discontinued"};
        // Form<Computer> computerForm = SecureForm.form(Computer.class).bindFromRequest(allowedFields);

        Form<Computer> computerForm = SecureForm.form(Computer.class).bindFromRequest();
        if(computerForm.hasErrors()) {
            return badRequest(
                SecureForm.signForms(
                    editForm.render(id, computerForm)
                )
            );
        }
        computerForm.get().update(id);
        return GO_HOME;
    }


How it works
------------
The value of the added field (```<input type="hidden" name="formSignature">```) contains a simple base64 encoded list of all
form fields that were present when sending the form to the client. To secure this list, a SHA256 hash of the field list,
salted with the _application.secret_ config value, is added.

When bind() is called and an entity (Model subclass*) is bound, SecureForm will check for the presence of either the allowedFields[] parameter or the form
signature field. If none is present or the form data does not match the form signature, it will throw a runtime error.


Automatic field list handling
-----------------------------
Field lists with brackets are adjusted automatically to allow for indexes, e.g.: ```<input name="field[]">``` in the form
sent to the client allows _field[0]_, _field[1]_ etc. to be submitted.


Wildcards for dynamic forms
---------------------------
In cases where you modify the form on the client-side, e.g. by JS, you can use the * wildcard:
```@formKey("myDynAddedField, myJSFieldX*, myWhat*Field")```


Exercise
--------
This project consists of the _Computer Database_ example which shipps with Play. A field "secure" was added to the
Computer entity and is shown in the list, but not in the add/edit forms. Now try to manipulate the form in the
browser to inject a value into the "secure" field.

The unsecured 'create new computer' form will let you do this easily. Now try the secured 'edit computer' form.

---
* Note: SecureForm checks in this example for play.db.ebean.Model instances - if you use JPA (as we do), you need to
adjust the check in SecureForm.validateSubmittedForm()

--
Bachi.
[www.insign.ch]


[www.insign.ch]:http://www.insign.ch
[Play 2 Framework]:http://www.playframework.org
[Parameter tampering]:https://www.owasp.org/index.php/Web_Parameter_Tampering