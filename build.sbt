import play.Project._

name := "computer-database"

version := "1.0"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  "org.jsoup" % "jsoup" % "1.7.3")

playJavaSettings
